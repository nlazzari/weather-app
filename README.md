An app that uses JSON data from the Open Weather Maps API, and icons from
weathericons.io to display the weather based on the user's current location.
The background colour changes based on temperature, and the temperature
displayed may be clicked to toggle units between metric and imperial.

(NOTE: Please use Firefox, as Chrome's geolocation is deprecated for
non-HTTPS sources)