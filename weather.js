var WeatherModule = (function() {

	//Module constants:
	var ICON_ID = "#icon",	//Icon element's id
		CITY_ID = "#city",	//Id for element displaying city
		TEMP_ID = "#temp",	//Id for element displaying temperature
		WRAPPER_ID = "#wrapper", //Id for wrapper div covering body (gradient overlay)

		TRANSP		= "0.60",
		WHITE		= "rgba(255, 255, 255, " + TRANSP + ")",	//rgb color code for white
		L_BLUE		= "rgba(200, 200, 255, " + TRANSP + ")",	//rgb color code for light blue
		BLUE 		= "rgba(100, 100, 255, " + TRANSP + ")",	//rgb color code for blue
		L_GREEN 	= "rgba(200, 255, 150, " + TRANSP + ")",	//rgb color code for light green
		GREEN 		= "rgba(150, 255, 0, "   + TRANSP + ")",	//rgb color code for green
		L_YELLOW	= "rgba(255, 255, 200, " + TRANSP + ")",	//rgb color code for light yellow
		YELLOW 		= "rgba(255, 255, 100, " + TRANSP + ")",	//rgb color code for yellow
		L_ORANGE	= "rgba(255, 200, 150, " + TRANSP + ")",	//rgb color code for light orange
		ORANGE 		= "rgba(255, 200, 100, " + TRANSP + ")",	//rgb color code for orange
		L_RED		= "rgba(255, 150, 100, " + TRANSP + ")",	//rgb color code for light red
		RED 		= "rgba(255, 100, 100, " + TRANSP + ")";	//rgb color code for white

	//Object used to store OpenWeatherMap JSON data
	var _owm = {
				API : "76e9d498b9b9160c12f5d84c7d8df822",	//OWM API key	
	    		url : "", 					// JSON AJAX url, to query OWM API
				units : 'metric',			// Desired Units for weather data ('metric', or 'imperial')
				lat : "40.7127",			// Latitude coordinate (default coordinate values: NYC, USA)
				lon : "-74.0059",			// Longitude coordinate (coordinates to be provided by geolocation service) 

	    		city : "", 					// City resolved from lat / lon by OWM
	    		country : "", 				// Country resolved from lat / lon by OWM
	    		weatherDescription : "",	// Description of weather
	    		weatherIcon : "",			// Icon code from OWM, use this code to get an icon with Weather Icons by Erik Flowers
	    		weatherId : "",				// ID code from OWM, sorts weather type into groups
	    		temp: "",					// Temperature from OWM (degrees C or F determined by units specified)
	    		
	    	}	

	// Weather object to return
	var weather = {
		myJSON : null	//OpenWeatherMap JSON response object
	};


	//Get user's coordinates from geolocation service,
	// and run callback function when successful (or even if it fails)
	function getCoordinates(callback) {

		// If geolocation service exists:
		if (navigator.geolocation) {
			
			//Get user position
			navigator.geolocation.getCurrentPosition( 

				//On success of getting position:
				function(position)   {
					// Get user's coordinates, and run callback
					_owm.lat = position.coords.latitude,
					_owm.lon = position.coords.longitude;

					if(callback && typeof callback === 'function') {
						callback();
					}
		    	
		    	
		    	},

		    	// On failure to get position (user chooses 'DENY'):
		    	function() {
		    		// Run callback with default valued coordinates
		    		if(callback && typeof callback === 'function') {
						callback();
					}

		  	});
		}
		//If geolocation service does not exist:
		else {
			console.log("Sorry, geolocation services are not available on your device.");
		}


	}	

	//Fetch Weather JSON data from OpenWeatherMap, and run callback when complete
	function getWeatherJSON(callback) {

		//Assemble OWM JSON API url with user position, API key, and desired units 
		_owm.url = 'http://api.openweathermap.org/data/2.5/weather?lat=' 
			   + _owm.lat + '&lon=' + _owm.lon + '&units=' + _owm.units + '&APPID=' + _owm.API;

		//Make AJAX request to OWM using JSON url
		$.getJSON(_owm.url, function(json) {

			//Parse data from JSON object received from OWM:
			_owm.city		 			= json.name,
			_owm.country				= json.sys.country,
			_owm.weatherDescription 	= json.weather[0].description,
			_owm.weatherIcon			= json.weather[0].icon,
			_owm.weatherId				= json.weather[0].id,
			_owm.temp 					= json.main.temp,
				
			output				= "",

			//Copy JSON object locally for debug purposes
			weather.myJSON				= json;

			/* Test output for debug only
			output = "<p>" + _owm.city + ", " + _owm.country + " has conditions of " + _owm.weatherDescription + ", with <br>" +
					 "a temperature of " + Math.round(_owm.temp) + " deg C.";

	    	$("#test").html( output );
			*/

	    	//If callback function is present, run it
	    	if(callback && typeof callback === 'function') {
				callback();
			}

	    } );

	}

	//Get current coordinates, and weather data
	function getWeather(callback) {

		//Get current user location, and when complete - 
		//get JSON weather data from OpenWeatherMaps
		getCoordinates( function() {
			getWeatherJSON(callback);
		});

	};

	// Adds a class to the icon display div by combining
	// weathericons.io's API class prefixes with
	// OpenWeatherMap's JSON weather ID codes,
	// which creates the weather icon
	function setIcon() {
		var iconClass;

		//Fix incorrect icon for 'mist', change to icon for 'fog'
		if(_owm.weatherId === 701) {
			_owm.weatherId = 741;
		}

		//Add prefix to OWM's code
		iconClass = "wi wi-owm-" + _owm.weatherId;
		//console.log(iconClass);

		//Clears current icon class, then adds the new icon class
		// reflecting the current weather
		$(ICON_ID).removeClass();
		$(ICON_ID).addClass(iconClass);


	}

	// Display city on page
	function setCity() {

		$(CITY_ID).text(_owm.city + ", " + _owm.country);
	}

	// Display temperature on page with desired units
	function setTemp() {
		var t   = Math.round(_owm.temp),
			far = " °F",
			cel = " °C",
			units = "";

		if(_owm.units === 'metric') {
			units = cel;	//use celsius for metric units
		}
		else {
			units = far;	//use fahrenheit for imperial units
		}

		t += units;

		$(TEMP_ID).text(t);

	}

	// Toggles units from metric (C) to imperial (F), for use on click event
	// on the temperature display
	weather.toggleUnits = function() {
		// Toggle units to opposite type than present,
		// perform unit conversion for new units
		if(_owm.units === 'metric') {
			_owm.units = 'imperial';
			_owm.temp  = (1.8*_owm.temp) + 32; //convert celsius to fahrenheit
		}
		else {
			_owm.units = 'metric';
			_owm.temp  = (_owm.temp - 32)/1.8; //convert fahrenheit to celsius
		}

		//Update displayed temperature
		setTemp();

	}

	// Sets the background linear gradient based on the current temperature
	function setGradient() {
		var gradient = "",		//css string for linear gradient
			temperature = -1,	//temperature in celsius
			top = "",			//top color in gradient
			bottom = "";		//bottom color in gradient

		//If temperature is currently in metric use that,
		//otherwise convert to metric
		if(_owm.units === 'metric') {
			temperature = _owm.temp;
		}
		else {
			temperature  = (_owm.temp - 32)/1.8; //convert fahrenheit to celsius
		}

		//Choose gradient colors based on temperature (deg C):
		if(temperature <= 0) {
			top 	= L_BLUE;
			bottom 	= WHITE;
		}
		else if(temperature > 0 && temperature < 10) {
			top 	= BLUE;
			bottom 	= L_BLUE;
		}
		else if(temperature >= 10 && temperature < 15) {
			top 	= BLUE;
			bottom 	= L_GREEN;
		}
		else if(temperature >= 15 && temperature < 20) {
			top 	= L_GREEN;
			bottom 	= YELLOW;
		}
		else if(temperature >= 20 && temperature < 25) {
			top 	= YELLOW;
			bottom 	= L_YELLOW;
		}
		else if(temperature >= 25 && temperature < 30) {
			top 	= ORANGE;
			bottom 	= YELLOW;
		}
		else if(temperature >= 30 && temperature < 40) {
			top 	= L_RED;
			bottom 	= ORANGE;
		}
		else if(temperature >= 40) {
			top		= RED;
			bottom	= ORANGE;
		}


		//Set the CSS gradient string:
		gradient = 'linear-gradient(' + top + ',' + bottom + ')';

		//Change the wrapper CSS to the desired gradient
		$(WRAPPER_ID).css('background-image', gradient);

	}

	 
	 //Exposed method to test setGradient()
	weather.changeTemp = function(temp) {
		_owm.temp = temp;

		setGradient();
		setTemp();
	}

	// Exposed method to test setIcon(). Example codes:
	//	313 - Shower rain and drizzle
	//	500 - Light rain
	//  521 - Shower rain
	//	602 - Heavy snow
	//  741 - Fog
	//	800 - Clear sky
	//	802 - Scattered clouds
	//	902 - Hurricane
	//	904 - Hot
	weather.changeIcon = function(code) {
		_owm.weatherId = code;
		setIcon();
	}
	//*/

	// Update's displayed information and aesthetic:
	// - icon glyph (by weathericons.io) based on OWM data
	// - city & country based on OWM data
	// - temperature from OWM
	// - gradient based on temperature
	function updateDisplay() {
		
		setIcon();
		setCity();
		setTemp();
		setGradient();
	}

	//Fetches weather, and updates displayed data on callback
	weather.run = function() {
		getWeather( updateDisplay );
	}

	// Return weather object 
	return weather;


}());  //END MODULE


WeatherModule.run();



